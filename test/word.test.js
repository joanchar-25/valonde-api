const expect = require('chai').expect
const supertest = require('supertest')
const api = supertest('http://localhost:4000')
/* eslint-disable no-unused-expressions */

describe('GET /iecho', () => {
  it('/iecho?text=test -> should return a 200 response', done => {
    api.get('/iecho?text=test')
      .set('Accept', 'application/json')
      .expect(200, done)
  })
  it('/iecho?text= -> should return a 400 response', done => {
    api.get('/iecho?text=')
      .set('Accept', 'application/json')
      .expect(400, done)
  })
  it('/iecho?text -> should return a 400 response', done => {
    api.get('/iecho?text')
      .set('Accept', 'application/json')
      .expect(400, done)
  })
  it('/iecho? -> should return a 400 response', done => {
    api.get('/iecho?')
      .set('Accept', 'application/json')
      .expect(400, done)
  })
  it('/iecho -> should return a 400 response', done => {
    api.get('/iecho?')
      .set('Accept', 'application/json')
      .expect(400, done)
  })
  it('/iecho?text=test -> should return an object', done => {
    api.get('/iecho?text=test')
      .set('Accept', 'application/json')
      .end((err, res) => {
        expect(err).to.be.null
        expect(res.body).to.be.an('object')
        done()
      })
  })
  it('/iecho?text=test -> should return an object that have a fields called "text" and "palindrome"', done => {
    api.get('/iecho?text=test')
      .set('Accept', 'application/json')
      .end((err, res) => {
        expect(err).to.be.null
        expect(res.body).to.have.property('text')
        expect(res.body).to.have.property('palindrome')
        done()
      })
  })
  it('/iecho?text= -> should return an object that have a field called "error"', done => {
    api.get('/iecho?text=')
      .set('Accept', 'application/json')
      .end((err, res) => {
        expect(err).to.be.null
        expect(res.body).to.have.property('error')
        done()
      })
  })
})
