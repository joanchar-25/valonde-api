const express = require('express')
const router = express.Router()
const ctrl = require('../controllers/wordController')

/* GET home page. */
router.get('/', function (req, res, next) {
  res.render('index', { title: 'Express' })
})

router.get('/iecho', ctrl)

module.exports = router
