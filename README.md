# VALONDE API

## Descrición

API que recibe un texto y responde con el mismo texto invertido. De igual manera identifica si el texto enviando corresponde a un palíndromo (palabra que se lee igual en un sentido que en otro).

## Tecnologías

Esta API fue implementada en Node 12.13.0, soportándose en diferentes paquetes y librerías para completar toda la funcionalidad requerida.

```
NodeJs
Express 
StandarJs
Mocha
Chai
Supertest
```

## Instalación y ejecución

Luego del clonado del repositorio, el API se puede instalar y ejecutar de dos maneras

### Utilizando NPM

Ejecute el siguiente comando, para realizar la instalación de todos los paquetes y librerías necesarias.

```
npm install
```

Una vez instaladas todas las dependencias, se debe ejecutar el siguiente comando para comenzar la ejecución del API

```
npm start
```

Por defecto, esto levantara el API en
```
http://localhost:4000
```

### Utilizando docker-compose

También se puede realizar la instalación de todos los paquetes y librerías necesarios para la correcta ejecución a través de docker-compose. Para ello se debe asegurar de tener instalado docker y docker-compose en su ambiente.
En caso de no ternerlo instalado, se recomienda leer el siguiente enlace

https://docs.docker.com/

Ejecute el siguiente comando, para realizar la instalación de todos los paquetes y librerías necesarias.

```
docker-compose run --rm api npm install
```

Una vez instaladas todas las dependencias, se debe ejecutar el siguiente comando para comenzar la ejecución del API

```
docker-compose up
```

Por defecto, esto levantara el API en
```
http://localhost:4000
```

Si se desea validar la correcta ejecución del contenedor Docker, se puede realizar a través del comando

```
docker ps
```

O en su defecto

```
docker-compose ps
```

## Pruebas automatizadas

Esta API cuenta con la implementación de pruebas automaticas con el uso de Mocha + Chai + Supertest. Para ejecutarlas solo es necesario correr el comando. Nota: Asegurese de que el API se encuentre ejecutando antes realizar este paso.

```
npm test
```

El cual mostrará los siguientes mensajes, si las pruebas se ejecutan de forma satisfactorias

```
> valonde-api@0.0.0 test /home/joanchar/projects/tests-interview/valonde-api
> mocha

  GET /iecho
    ✔ /iecho?text=test -> should return a 200 response
    ✔ /iecho?text= -> should return a 400 response
    ✔ /iecho?text -> should return a 400 response
    ✔ /iecho? -> should return a 400 response
    ✔ /iecho -> should return a 400 response
    ✔ /iecho?text=test -> should return an object
    ✔ /iecho?text=test -> should return an object that have a fields called "text" and "palindrome"
    ✔ /iecho?text= -> should return an object that have a field called "error"
```

## Funcionalidad

Esta API posee solo un enpoint implementado, el cual espera a través de una petición GET el paremtro "text" con la cadena de caracteres que se desea invertir.

Una vez que dicha cadena de caracteres es invertida, se realiza la verificación de si la misma corresponde a un palíndromo.

Ejemplos usando curl:

```
curl -v -X GET "http://localhost:4000/iecho?text=test" -H "accept: application/json"

* Trying 127.0.0.1:4000...
* TCP_NODELAY set
* Connected to localhost (127.0.0.1) port 4000 (#0)
> GET /iecho?text=test HTTP/1.1
> Host: localhost:4000
> User-Agent: curl/7.68.0
> accept: application/json
> 
* Mark bundle as not supporting multiuse
< HTTP/1.1 200 OK
< X-Powered-By: Express
< Access-Control-Allow-Origin: *
< Content-Type: application/json; charset=utf-8
< Content-Length: 34
< ETag: W/"22-ODZjDNZ5t7cLvsTZ2EIjD4YjNds"
< Date: Wed, 22 Sep 2021 02:26:49 GMT
< Connection: keep-alive
< 
* Connection #0 to host localhost left intact
{"text":"tset","palindrome":false}
```

De igual manera, si el endpoint no recibe una cadena en el parámetro "tex"

```
curl -v -X GET "http://localhost:4000/iecho?text=" -H "accept: application/json"

Note: Unnecessary use of -X or --request, GET is already inferred.
* Trying 127.0.0.1:4000...
* TCP_NODELAY set
* Connected to localhost (127.0.0.1) port 4000 (#0)
> GET /iecho?text= HTTP/1.1
> Host: localhost:4000
> User-Agent: curl/7.68.0
> accept: application/json
> 
* Mark bundle as not supporting multiuse
< HTTP/1.1 400 Bad Request
< X-Powered-By: Express
< Access-Control-Allow-Origin: *
< Content-Type: application/json; charset=utf-8
< Content-Length: 19
< ETag: W/"13-/ig203f/evdylbGpofCLDctHxvE"
< Date: Wed, 22 Sep 2021 02:28:45 GMT
< Connection: keep-alive
< 
* Connection #0 to host localhost left intact
{"error":"no text"}
```
