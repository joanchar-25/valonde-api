const investText = (req, res, next) => {
  if (req.query && req.query.text) {
    const text = req.query.text
    const invertedText = text.split('').reverse().join('')
    res.json({
      text: invertedText,
      palindrome: text.length > 1 && text === invertedText
    })
  } else {
    res.status(400).json({
      error: 'no text'
    })
  }
}

module.exports = investText
